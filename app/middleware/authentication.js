const jwt = require('jsonwebtoken');
const util = require('util');
const key = require('../config/secret.js');
const connection = require('../db/connection.js');

const query = util.promisify(connection.query).bind(connection);

class Authentication{
    static async auth(req, res, next) {
        try {
            const token = req.body.token;
            if (!token) return next(new Error('Not found authentication'));

            const tokens = token.split('Bearer ');
            if (tokens.length !== 2 || tokens[0] !== '') {
                return next(new Error('Not authentication format'));
            }
            const authToken = tokens[1];
            const data = await jwt.verify(authToken, key.sceret);
            const sql = `SELECT * FROM user WHERE idUser = ${data.idUser}`;
            
            await connection.query(sql, (err, result) => {
                if (err) return next(new Error(err));
                const user = JSON.parse(JSON.stringify(result));
                if (user.length == 0) {
                    return next(new Error('User is not found!'));
                }
                req.user = user[0];
                return next();
            });
        } catch (error) {
            return next(error);
        }
    }
    static async checkAuthorPost(req, res, next) {
        try {
            const author = req.user;
            const idUser = author.idUser;
            const { id } = req.params;
            const sql = `SELECT idUser FROM post WHERE post_id = ${id}`;
            const result = await query(sql);
            if (!result) return next(new Error('False'));
            const idUserPost = result[0].idUser;
            if (idUser !== idUserPost) return next(new Error("You can't not update and delete this post!!"));
            return next();

        } catch (error) {
            return next(error);
        }
    }
    static async checkAuthorComment(req, res, next) {
        const author = req.user;
        const idUser = author.idUser;
        const { id } = req.params;
        const sql = `SELECT idUser FROM comment WHERE comment_id = ${id}`;
        const result = await query(sql);
        const idUserComment = result[0].idUser;
        if (idUser !== idUserComment) return next(new Error("You can't not update and delete this comment"));
        return next();
    }
    static async checkAuthorReply(req, res, next) {
        const author = req.user;
        const author_id = author.idUser;
        const reply_id = req.params;
        const sql = `SELECT idUser FROM relpy WHERE reply_id = ${reply_id}`;
        const result = await query(sql);
        const idUserReply = result[0].idUser;
        if (author_id !== idUserReply) return next(new Error("You can't update and delele this post!"));
        return next();
    }
}

module.exports  = Authentication;