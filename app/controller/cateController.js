const connection = require('../db/connection.js');
const util = require('util');

const query = util.promisify(connection.query).bind(connection);

class Category {
    static async addCate (req, res, next) {
        try {
            const { name } = req.body;
            const sql = `INSERT INTO cate  ( cat_name ) VALUE ('${name}')`;
            await connection.query(sql);
            return res.json({
                message: 'Query is success!',
            });
        } catch (error) {
            return next(error);
        }
    }

    static async updateCate (req, res, next ){
        try {
            const { id } = req.params;
            const { name } = req.body;
            const sql = `update cate set cat_name = '${name}' where cat_id = '${id}'`;
            await connection.query(sql);
            return res.json({
                message: 'Update successfully!'
            });
        } catch (error) {
            return next(error);
        }
    }

    static async deleteCate(req, res, next) {
        try {
            const { id } = req.params;
            const sql = `DELETE FROM cate WHERE cat_id = ${id}`;
            await connection.query(sql);
            return res.json({
                message: 'Delele sucessfully!'
            });
        } catch (error) {
            return next(error);
        }
    }

    static getAll(req, res, next) {
        try {
            const sql = "SELECT * FROM cate";
            const result = query(sql);
            if (!result) return next(new Error('False'));
            return res.json({
                message: 'Successfully',
                cate: result
            });
        } catch (error) {
            return next(error);
        }
    }
}

module.exports = Category;