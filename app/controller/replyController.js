const connection = require('../db/connection.js');
const  util = require('util');

const query = util.promisify(connection.query).bind(connection);

class Reply{
    static async addReply(req, res, next) {
        try {
            const author = req.user;
            const idUser = author.idUser;
            const post_id = req.params.id;
            const content = req.body.content;
            if (!content) return next(new Error('Content is empty'));
            const value = [
                [
                    post_id,
                    idUser, 
                    content
                ]
            ];
            const sql = `INSERT INTO relpy (post_id, idUser, content) VALUES ?`;
            await connection.query(sql, [value]);
            return res.json({
                message: 'Query is success',
            })
        } catch (error) {
            return next(error);
        }
    }
    static async getAllReplies(req, res, next){
        try {
            const sql = 'SELECT relpy.reply_id, relpy.idUser, relpy.content, relpy.timestamp, user.name FROM relpy INNER JOIN user ON relpy.idUser = user.idUser';
            const result = await query(sql);
            if (!result) return next(new Error('False'));
            return res.json({
                message: 'Successfully!',
                relies: result
            });
        } catch (error) {
            
        }
    }
    static async updateReply(req, res, next) {
        try {
            const { id } = req.params;
            const content = req.body.content;
            const sql1 = `UPDATE relpy SET content = '${content}' WHERE reply_id = ${id}`;
            await connection.query(sql1);
            return res.json({
                message: 'Successfully!'
            });
        } catch (error) {
            return next(error);
        }
    }
    static async deleteReply(req, res, next) {
        try {
            const { id } = req.params;
            const sql1 = `DELETE FROM relpy WHERE reply_id = ${id}`;
            const sql2 = `DELETE FROM comment WHERE reply_id = ${id}`;
            await Promise.all([
                connection.query(sql1),
                connection.query(sql2)    
            ]);
            return res.json({
                message: 'Successfully'
            });
        } catch (error) {
            return next(error);
        }
    }
}

module.exports = Reply;