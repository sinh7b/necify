const util = require('util');
const connection = require('../db/connection.js');

const query = util.promisify(connection.query).bind(connection);
class Comment {
    static async addComment (req, res, next) {
        try {
            const author = req.user;
            const idUser = author.idUser;
            let post_id, reply_id;
            if (req.params.post_id) post_id = req.params.post_id;
            else post_id = 0;
            if (req.params.reply_id) reply_id = req.params.reply_id;
            else reply_id = 0; 
            const { content} = req.body;
            const value = [
                [
                    post_id,
                    reply_id,
                    idUser, 
                    content,
                ]
            ];
            const sql = 'INSERT INTO comment (post_id, reply_id, idUser, content) VALUES ?';
            await connection.query(sql, [value]);
            return res.json({
                message: 'Query is success!',
            });
        } catch (error) {
            return next(error);
        }
    }
    static async updateComment(req, res, next) {
        try {
            const { id } = req.params;
            const { content } = req.body;
            const sql = `UPDATE comment SET content = '${content}' WHERE comment_id = ${id}`; 
            await connection.query(sql);
            return res.json({
                message: 'Successfully'
            });
        } catch (error) {
            return next(error);
        }
    }
    static async deleteComment(req, res, next) {
        try {
            const { id } = req.params;
            const sql = `DELETE FROM comment WHERE comment_id = ${id}`; 
            await connection.query(sql);
            return res.json({
                message: 'Successfully'
            });
        } catch (error) {
            return next(error);
        }
    }
    static getAllComments(req, res, next) {
        try {
            const sql = 'SELECT comment.comment_id, user.name, comment.content, comment.timestamp FROM comment INNER JOIN user ON comment.idUser = user.idUser';
            const result = query(sql);
            if (!result) return next(new Error('Query is false!'));
            return res.json({
                message: 'Successfully',
                comment: result
            });
        } catch (error) {
            return next(error);
        }
    }
}

module.exports = Comment;