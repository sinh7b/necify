const util = require('util');
const connection = require('../db/connection.js');

const query = util.promisify(connection.query).bind(connection);
class Rating{
    static async addRating(req, res, next) {
        try {
            const author = req.user;
            const idUser = author.idUser;
            const post_id = req.params.id;
            const { score } = req.body;
            const sql1 = `SELECT * FROM rate WHERE idUser = ${idUser} AND post_id = ${post_id}`;
            const result = await query(sql1);
            if (result.length > 0){
                const sql2 = `UPDATE rate SET score = ${score} WHERE idUser = ${idUser} AND post_id = ${post_id}`;
                await connection.query(sql2);
            }
            const value = [
                [
                    idUser, 
                    post_id, 
                    score   
                ]
            ];
            const sql = `INSERT INTO rate (idUser, post_id, score) VALUES ?`;
            await connection.query(sql, [value]);
            return res.json({
                message: 'Successfully!'
            });
        } catch (error) {
            return next(error);
        }
    }
    static async getScore(req, res, next){
        try {
            const post_id = req.params.id;
            const result = await query(`SELECT AVG(score) AS score FROM rate WHERE post_id = ${post_id}`);
            if (!result) return next(new Error('isFalse'));
            const score = Math.round(result[0].score * 10)/10;
            return res.json({
                message: 'Success', 
                avg: score
            });
        } catch (error) {
            return next(error);
        }
    }
    static async getRate(req, res, next) {
        try {
            const post_id = req.params.id;
            const sql = `SELECT user.name, rate.post_id, rate.score FROM rate INNER JOIN user ON user.idUser = rate.idUser WHERE post_id = ${post_id}`;
            const result = await query(sql);
            if (!result) return next(new Error('Fasle'));
            return res.json({
                message: 'Success', 
                rate
            });
        } catch (error) {
            return next(error);
        }
    }
}

module.exports = Rating;