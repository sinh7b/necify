const connection = require('../db/connection.js');
const jwt = require('jsonwebtoken');
const key = require('../config/secret.js');
const util = require('util');

const query = util.promisify(connection.query).bind(connection);
class User {
    static async addUser(req, res, next){
        try {
            const {name, email, password} = req.body;
            const VALUES = [
                [
                    name,
                    email,
                    password
                ]
            ];
            const sql1 = "INSERT INTO user (name, email, password) VALUES ?";
            await connection.query(sql1, [VALUES]);
            return res.json({
                message: 'Query is success!'
            });
        } catch (error) {
            return next(error);
        }
    }

    static async getUsers(req, res, next) {
        try {
            const result = query("Select * from user");
            if (!result) return next(new Error('false'));
            return res.json({
                message: 'Sucessfully',
                user: result
            });
        } catch (error) {
            return next(error);
        }
    }



    static async updateUser(req, res, next) {
        try {  
            const data = req.user;
            const id = data.idUser;
            const name = req.body.name;
            const sql1 = `update user set name = '${name}' where idUser = ${id}`; 
            await connection.query(sql1);
            return res.json({
                message: 'Successfully'
            });
        } catch (error) {
            return next(error);
        }
    }
    static async login(req, res, next) {
        try {
            const { username, password } = req.body;
            if (!username) return next(new Error('Email is empty!'));
            if (!password) return next(new Error('Password is empty!'));
            const sql = `SELECT * FROM user WHERE email = '${username}'`;
            connection.query(sql, async (err, result) => {
                if (err) throw err;
                const user = JSON.parse(JSON.stringify(result));
                console.log(user);
                if (user.length != 0){
                    if (user[0].password === password) {
                        const users = Object.assign({}, user[0]);
                        const token = await jwt.sign(users, key.sceret);
                        return res.json({
                            isSuccess: 'true',
                            user: result,
                            token
                        })
                    }else return next(new Error('Password was wrong!'));
                }else return next(new Error('Email not found!'));
            });
        } catch (error) {
            return next(error);
        }
    }
}

module.exports = User;