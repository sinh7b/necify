const util = require('util');
const connection = require('../db/connection.js');

const query = util.promisify(connection.query).bind(connection);
class Post{
    static async addPost(req, res, next) {
        try {
            const data = req.user;
            const idUser = data.idUser;
            const { cat_id, title, content} = req.body;
            const value = [
                [
                    cat_id,
                    idUser,
                    title,
                    content
                ]
            ];
            const sql = "INSERT INTO post (cat_id, idUser, title, content) VALUES ?";
            await connection.query(sql, [value]);
            return res.json({
                message: 'Query is success!'
            });
        } catch (error) {
            return next(error);
        }
    }
    static async getPost(req, res, next) {
        try {
            const { id } = req.params;
            const result = await query(`SELECT * FROM post WHERE post_id = ${id}`);
            if (!result) return next(new Error('Query is false'));
            return res.json({
                post: result
            });
        } catch (error) {
            return next(error);
        }
    }
    static async getAllPost(req, res, next) {
        try {
            const result = await query("SELECT p.post_id, p.title, p.content, u.name, c.cat_name FROM post AS p INNER JOIN user AS u ON p.idUser = u.idUser INNER JOIN cate AS c ON p.cat_id = c.cat_id");
            if (!result) return next(new Error('Query is false!'));
            return res.json({
                posts: result
            });
        } catch (error) {
            return next(error);
        }
    }
    static async updatePost(req, res, next) {
        try {
            const { id } = req.params;
            const { title, content } = req.body;
            const sql = `UPDATE post set title = '${title}', content ='${content}' WHERE post_id = ${id}`;
            const result = await     query(sql);
            if (!result) return next(new Error('Query is false'));
            return res.json({
                message: 'Success!'
            });
        } catch (error) {
            return next(error);
        }
    }
    static async deletePost(req, res, next) {
        try {
            const { id } = req.params;
            const sql = `DELETE FROM post WHERE post_id = ${id}`;
            const sql1 = `DELETE FROM comment WHERE post_id = ${id}`;
            const sql2 = `DELETE FROM rate WHERE post_id = ${id}`;
            const sql3 = `DELETE FROM relpy WHERE post_id = ${id}`;
            await Promise.all([
                connection.query(sql),
                connection.query(sql1),
                connection.query(sql2),
                connection.query(sql3)
            ]);
            return res.json({
                message: 'Delete successfully'
            })
            
        } catch (error) {
            return next(error);
        }
    }
}

module.exports = Post;