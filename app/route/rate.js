const express = require('express');
const router = express.Router();
const auth = require('../middleware/authentication.js');
const RatingController = require('../controller/rateController.js');

router.post('/add-rating/:id', [auth.auth], RatingController.addRating);
router.get('/get-score/:id', RatingController.getScore);
router.get('/get-rate/:id', RatingController.getRate);

module.exports = router;