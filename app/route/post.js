const express = require('express');
const router = express.Router();
const PostController = require('../controller/postController.js');
const auth = require('../middleware/authentication.js');

router.post('/add-post', [auth.auth], PostController.addPost);
router.get('/get-post/:id', PostController.getPost);
router.get('/get-all-post', PostController.getAllPost);
router.put('/update-post/:id', [auth.auth, auth.checkAuthorPost], PostController.updatePost);
router.delete('/delete-post/:id', [auth.auth, auth.checkAuthorPost], PostController.deletePost);

module.exports = router;