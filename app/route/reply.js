const express = require('express');
const router = express.Router();
const auth = require('../middleware/authentication.js');
const ReplyController = require('../controller/replyController.js');

router.post('/add-reply/:id', [auth.auth], ReplyController.addReply);
router.get('/get-all-replies', [auth.auth], ReplyController.getAllReplies);
router.put('/update-reply/:id', [auth.auth, auth.checkAuthorReply], ReplyController.updateReply);
router.delete('/delete-reply/:id', [auth.auth, auth.checkAuthorReply], ReplyController.deleteReply);

module.exports = router;