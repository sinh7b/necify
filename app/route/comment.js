const express = require('express');
const router = express.Router();
const CommentController = require('../controller/commentController.js');
const auth = require('../middleware/authentication.js');

router.post('/add-comment-post/:post_id', [auth.auth], CommentController.addComment);
router.post('/add-comment-reply/:reply_id', [auth.auth], CommentController.addComment);
router.put('/update-comment/:id', [auth.auth, auth.checkAuthorComment], CommentController.updateComment);
router.delete('/delete-comment/:id', [auth.auth, auth.checkAuthorComment], CommentController.deleteComment);
router.get('/get-all-comments', CommentController.getAllComments);

module.exports = router;