const express = require('express');
const router = express.Router();
const CateController = require('../controller/cateController.js');

router.post('/add-cate', CateController.addCate);
router.put('/update-cate/:id', CateController.updateCate);
router.delete('/delete-cate/:id', CateController.deleteCate);
router.get('/get-all-cates', CateController.getAll);

module.exports = router;