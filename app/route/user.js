const express = require('express');
const router = express.Router();
const UserController = require('../controller/userController.js');
const auth = require('../middleware/authentication.js');

router.get('/get-users', [auth.auth], UserController.getUsers);
router.post('/add-user', UserController.addUser);
router.put('/update-user', [auth.auth], UserController.updateUser);
router.post('/login', UserController.login);
module.exports = router;