const mysql = require('mysql');

const conn = mysql.createConnection({
    database: 'Test',
    host: 'localhost',
    user: 'root',
    password: ''
})

module.exports = conn;