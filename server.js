const express = require('express');
const bodyParser = require('body-parser');

const connection = require('./app/db/connection.js');

const user = require('./app/route/user.js');
const cate = require('./app/route/cate.js');
const post = require('./app/route/post.js');
const comment = require('./app/route/comment.js');
const reply = require('./app/route/reply.js');
const rate = require('./app/route/rate.js');

const app = express();
const port = 3000;

connection.connect((err) => {
    if (err) throw err;
    console.log('connected!');
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));


app.use('/user', user);
app.use('/cate', cate);
app.use('/post', post);
app.use('/comment', comment);
app.use('/reply', reply);
app.use('/rate', rate);

app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});